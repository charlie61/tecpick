<?php
/**
 *
 */

namespace components\controllers;


use components\models\Profile;
use core\Controller;

class ProfileController extends Controller {


    public function indexAction(){

        $profile = new Profile();

        $uri = $_SERVER['REQUEST_URI'];
        $requestedUser = substr($uri, 20);
        $requestedUser = str_replace('/', '', $requestedUser);
        $_SESSION['requestedUser'] = $requestedUser;


        if($_SESSION['user'] === $_SESSION['requestedUser']){

            $profile->getOldData();
        }

        $this->setTitle('Данные');
    }

}