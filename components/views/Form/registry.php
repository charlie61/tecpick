<div class="container-fluid">
    <div class="row justify-content-center text-center">
        <?php if(isset($_SESSION['errors'])): ?>
        <div class="alert alert-danger">
            <ul>
                <?php foreach ($_SESSION['errors'] as $item=>$value):?>
                <li><?=$value; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <?php endif;?>
        <?php; ?>
        <?php if(isset($_SESSION['registry'])):?>
        <div class="alert alert-success">
            <p>Ваши данные успешно внесены!</p>
        </div>
        <?php endif;?>
    </div>
    <div class="row justify-content-center text-center">
        <div class="col-md-6">
            <?php if(isset($_SESSION['user']) && isset($_SESSION['registry'])):?>
            <a class="btn btn-primary registry" href="<?=PATH; ?>/my_account/<?=$_SESSION['user']; ?>" role="button">Редактировать</a>
            <?php endif; unset($_SESSION['registry']);?>
            <?php if(isset($_SESSION['errors'])):?>
                <a class="btn btn-primary registry" href="<?=PATH; ?>/my_account" role="button">Редактировать</a>
            <?php endif; unset($_SESSION['errors']);?>
        </div>
        <div class="col-md-6">
            <a class="btn btn-primary registry" href="" role="button">Ок</a>
        </div>
    </div>
</div>