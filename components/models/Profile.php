<?php
/**
 *
 */

namespace components\models;


use core\Model;
use core\Db;

class Profile extends Model {

    public $oldData = [];

    public function getOldData(){
       $result = Db::$connections->query("SELECT *
                                            FROM `users`
                                            WHERE `user_login` = '{$_SESSION['user']}';
                                                ");
       $oldUserData = $result->fetch();
        $_SESSION['name'] = $oldUserData['user_name'];
        $_SESSION['email'] = $oldUserData['user_email'];
        $_SESSION['avatar'] = $oldUserData['user_avatar'];
        $_SESSION['phone1'] = $oldUserData['user_phone1'];
        $_SESSION['phone2'] = $oldUserData['user_phone2'];
        $_SESSION['city'] = $oldUserData['user_city'];

    }
}