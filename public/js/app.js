window.addEventListener('DOMContentLoaded', function () {
    let avatar = document.getElementById('avatar');
    let image = document.getElementById('image');
    let input = document.getElementById('input');
    let image_name ='';
    let previews = document.querySelectorAll('.preview');
    let $modal = $('#modal');
    let formData = new FormData();
    let cropper;
    let form = $('form');

    $('#phone1').mask("(999)99-99-999");
    $('#phone2').mask("(999)99-99-999");

    input.addEventListener('change', function (e) {
        image_name = input.files[0].name;
        let files = e.target.files;
        let done = function (url) {
            input.value = '';
            image.src = url;
            $modal.modal('show');
        };
        let reader;
        let file;
        let URL; //!

        if (files && files.length > 0) {
            file = files[0];

            if (URL) {
                done(URL.createObjectURL(file));
            } else if (FileReader) {
                reader = new FileReader();
                reader.onload = function (e) {
                    done(reader.result);
                };
                reader.readAsDataURL(file);
            }
        }
    });

    $modal.on('shown.bs.modal', function () {
        cropper = new Cropper(image, {
            aspectRatio: 1,
            viewMode: 1,
            preview: previews,

        });
    }).on('hidden.bs.modal', function () {

        cropper.destroy();
        cropper = null;
    });

    document.getElementById('crop').addEventListener('click', function () {
        let canvas;

        $modal.modal('hide');

        if (cropper) {
            canvas = cropper.getCroppedCanvas({
                width: 200,
                height: 200,
            });
            avatar.src = canvas.toDataURL();
            canvas.toBlob(function (blob) {
                formData.set('avatar', blob, image_name);
            });
        }
    });





    $(function () {
        $('form').validate({
            rules: {
                name:{
                    required: true,
                    rangelength: [3, 20],
                    nameRus:true
                },
                login:{
                    required:true,
                    rangelength: [3, 20],
                    myLogin:true

                },
                email:{
                    required:true,
                    email:true
                },
                password:{
                    required:true,
                    myPass:true
                },
                city:{
                    required: true,
                    rangelength: [3, 20],
                    nameRus:true
                },
                phone1:{
                    required:true,
                    myPhone:true
                },
                phone2:{
                    myPhone2:true
                }
            },
            messages:{
                name:{
                    required:"Поле обязательно к заполнению",
                    rangelength:"Не менее 3 и не более 20 символов"
                },
                login:{
                    required:"Поле обязательно к заполнению",
                    rangelength:"Не менее 3 и не более 20 символов"
                },
                email:{
                    required:"Поле обязательно к заполнению",
                    email:"Введите корректный адрес"
                },
                password:{
                    required:"Поле обязательно к заполнению"
                },
                city:{
                    required:"Поле обязательно к заполнению",
                    rangelength:"Введите корректный адрес"
                },
                phone1:{
                    required:"Поле обязательно к заполнению",
                }
            },
            submitHandler: function(){
                sendMyForm();
            }
        })

    });

    jQuery.validator.addMethod("nameRus", function(value, element){
        return this.optional(element) || /^([-А-Яа-я\s]+)$/.test(value);
    }, "Ввод только буквами кирилицы!");

    jQuery.validator.addMethod("myLogin", function(value, element){
        return this.optional(element) || /^([-0-9A-Za-z_]+)$/.test(value);
    }, "Латиница, цифры, разрешены символы '-' и '_'.");

    jQuery.validator.addMethod("myPass", function(value, element){
        return this.optional(element) || /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[-_+#$])([-a-zA-Z0-9+_#$]{8,30})$/.test(value);
    }, "Мин. 8 символов: мин. 1 заглавная буква, 1 цифра и 1 спец. символ");

    jQuery.validator.addMethod("myPhone", function(value, element){
        return this.optional(element) || /^\(([0-9]{3})\)([0-9]{2})\-([0-9]{2})\-([0-9]{3})$/.test(value);
    }, "Введите ваш номер телефона в виде: (XXX)XX-XX-XXX");

    jQuery.validator.addMethod("myPhone2", function(value, element){
        return this.optional(element) || /^()|\(([0-9]{3})\)([0-9]{2})\-([0-9]{2})\-([0-9]{3})$/.test(value);
    }, "Введите ваш номер телефона в виде: (XXX)XX-XX-XXX");

    function sendMyForm(){

        let name = document.querySelector('#name').value;
        let login = document.querySelector('#login').value;
        let email = document.querySelector('#email').value;
        let password = document.querySelector('#password').value;
        let gender = document.querySelector('#gender').value;
        let city = document.querySelector('#city').value;
        let phone1 = document.querySelector('#phone1').value;
        let phone2 = document.querySelector('#phone2').value;

        formData.append("name", name);
        formData.append('login', login);
        formData.append('email', email);
        formData.append('password', password);
        formData.append('gender', gender);
        formData.append('city', city);
        formData.append('phone1', phone1);
        formData.append('phone2', phone2);

        $.ajax({
            url:'techpic/my_account',
            data:formData,
            type:'POST',
            async:true,
            // dataType:"json",
            contentType:false,
            processData:false,
            beforeSend:function(){
                // console.log('отправка начата');
            },
            success:function(res){
                // console.log('отправка успешна!');
                showRegistry(res);

            },
            complete:function(){
                // console.log('Закончено!');
            },
            error:function(xhr,ajaxOptions,thrownError){
                console.log(thrownError);
            }

        })


    }

    function showRegistry(res){
        $('.wrapper').html(res);
    }

});
