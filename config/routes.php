<?php
/**
 *
 */

use core\Router;

Router::add('^my_account/record$', ['controller' => 'Form', 'action' => 'record']);

Router::add('^my_account/([0-9A-Za-z-_]+)$', ['controller' => 'Profile', 'action' => 'index']);

Router::add('^my_account$', ['controller' => 'Form', 'action' => 'index']);
Router::add('^$', ['controller' => 'Main', 'action' => 'index']);
